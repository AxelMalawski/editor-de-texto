package TrabajoIndividualProcesadorDeTexto;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;

public class ProcesadorDeTexto {

 public static void main(String[] args) {
  marcoProcesador marco = new marcoProcesador();
  marco.setVisible(true);
  marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  
 }

}

class marcoProcesador extends JFrame{
 
	
	
 public marcoProcesador(){
  setTitle("Procesador de texto LABII");
  setBounds(450,250,400,300);
  panelProcesador panel = new panelProcesador();
  add(panel);
  
 
 }
 
}

class panelProcesador extends JPanel{
 
 JTextArea texto = new JTextArea();
 String letraFuente = "Dialog";
 int letraEstilo = Font.PLAIN;
 int letraTamagno = 12;
 private static String direccion = "";
 private static String direccionStyle = "";


 public panelProcesador(){
  
  setLayout(new BorderLayout());
 

  
  JMenuBar menu = new JMenuBar();
  
  JMenu fuente = new JMenu("Fuente");
  JMenu estilo = new JMenu("Estilo");
  JMenu tama�o = new JMenu("Tama�o");
  JMenu archivo = new JMenu("Archivo");
  
  menu.add(fuente);
  menu.add(estilo);
  menu.add(tama�o);
  menu.add(archivo);
  
  JMenuItem arial = new JMenuItem("Arial");
  JMenuItem courier = new JMenuItem("Courier");
  JMenuItem verdana = new JMenuItem("Verdana");
  JMenuItem negrita = new JMenuItem("Negrita");
  JMenuItem cursiva = new JMenuItem("Cursiva");
  JMenuItem ocho = new JMenuItem("8");
  JMenuItem diez = new JMenuItem("10");
  JMenuItem doce = new JMenuItem("12");
  JMenuItem catorce = new JMenuItem("14");
  JMenuItem dieciseis = new JMenuItem("16");
  JMenuItem abrir = new JMenuItem("Abrir");
  JMenuItem guardar = new JMenuItem("Guardar");
  
  
  fuente.add(arial);
  fuente.add(courier);
  fuente.add(verdana);
  estilo.add(negrita);
  estilo.add(cursiva);
  tama�o.add(ocho);
  tama�o.add(diez);
  tama�o.add(doce);
  tama�o.add(catorce);
  tama�o.add(dieciseis);
  archivo.add(abrir);
  archivo.add(guardar);
  
  JScrollPane scroll = new JScrollPane (texto, 
		    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
  
  
  arial.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraFuente = "Arial";
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // arial
  
  courier.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraFuente = "Courier";
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // courier
  
  verdana.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraFuente = "Verdana";
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // verdana
  
  negrita.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    if (letraEstilo == Font.ITALIC){
     letraEstilo = 3;
    } else
    {
     letraEstilo = Font.BOLD;
    }
    
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // negrita 
  
  cursiva.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    if (letraEstilo == Font.BOLD){
     letraEstilo = 3;
    } else
    {
     letraEstilo = Font.ITALIC;
    }
    
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // cursiva
  
  ocho.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraTamagno = 8;
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // ocho
  
  diez.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraTamagno = 10;
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // diez
  
  doce.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraTamagno = 12;
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // doce
  
  catorce.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraTamagno = 14;
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // catorce
  
  dieciseis.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
    letraTamagno = 16;
    texto.setFont(new Font(letraFuente , letraEstilo , letraTamagno));
    
   }
     
  }); // dieciseis
  texto = new JTextArea();
    
  add(menu , BorderLayout.NORTH);
  add(texto , BorderLayout.CENTER);
 
  abrir.addActionListener(new ActionListener(){

   public void actionPerformed(ActionEvent arg0) {
    
	   try {
           JFileChooser seleccionar = new JFileChooser();
           seleccionar.showOpenDialog(texto);
           String archivo = seleccionar.getSelectedFile().getAbsolutePath();
           String archivoSyle = seleccionar.getCurrentDirectory() +"\\"+ seleccionar.getSelectedFile().getName() + "Style";
           ObjectInputStream lectura = new ObjectInputStream(new FileInputStream(archivo));
           ObjectInputStream lecturaStyle = new ObjectInputStream(new FileInputStream(archivoSyle));
           texto.setText((String) lectura.readObject());
           texto.setDocument((StyledDocument) lecturaStyle.readObject());
           lectura.close();
           lecturaStyle.close();
       }
       catch (IOException ioe){
           JOptionPane.showConfirmDialog(null, "Error al cargar archivo", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
           ioe.printStackTrace();
           return;
       }
       catch (ClassNotFoundException c){
           System.out.println("Error, Clase no encontrada");
           c.printStackTrace();
           return;
       }
   }

    
   }
     
  ); //abrirArchivo
  
  guardar.addActionListener(new ActionListener(){

	   public void actionPerformed(ActionEvent arg0) {
	    
		   try{
               System.out.println("Guardado");
               JFileChooser Archivo = new JFileChooser();
               Archivo.showSaveDialog(texto);
               File archivo = (File) Archivo.getCurrentDirectory();
               String ruta = archivo+"\\"+Archivo.getSelectedFile().getName();
               String rutaStyle = archivo+"\\"+Archivo.getSelectedFile().getName()+"Style";
               direccion = ruta;
               direccionStyle = rutaStyle;
               ObjectOutputStream Salida = new ObjectOutputStream(new FileOutputStream(ruta) );
               ObjectOutputStream SalidaStyles = new ObjectOutputStream(new FileOutputStream(rutaStyle));
               Salida.writeObject(texto.getText());
               SalidaStyles.writeObject(texto.getDocument());
               Salida.close();
               SalidaStyles.close();
               JOptionPane.showConfirmDialog(null,"Archivos Guardados","Guardado Correcto",JOptionPane.CLOSED_OPTION,JOptionPane.PLAIN_MESSAGE);

           }
           catch (FileNotFoundException e){
               JOptionPane.showConfirmDialog(null, "Archivo no encontrado!", "Error! en Guardado", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
               System.out.println("Error en Guardado");
               e.printStackTrace();
           } catch (IOException e){
               e.printStackTrace();
           }
	    
	   }
	     
	  }); //GuardarArchivo
  
 } // public
 
 
 
} // class panelProcesador